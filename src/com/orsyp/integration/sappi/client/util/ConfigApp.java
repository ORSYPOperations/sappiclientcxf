package com.orsyp.integration.sappi.client.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class ConfigApp {
	
	public static void main(String[] args) {
		String configFile = "connection.conf";
		if (args.length>0)
			configFile = args[0];
		File f = new File(configFile);
		Scanner s = new Scanner(System.in);
		if (f.exists()) {
			System.out.println("Reading configuration file: " + configFile + " ...");
			CustomProperties prop = new CustomProperties(configFile);		
			String user =prop.getProperty("user");
			if (user==null) {
				System.out.println("User not found, invalid connection configuration file");
				return;
			}
			else
				System.out.println("Insert password for SAP/PI user " + user);
			String pw = s.nextLine();
			try {
				PrintWriter output = new PrintWriter(new FileWriter(configFile, true));
				output.println("");
				output.println("");
				output.println("# encrypted password");
				output.println("password="+Codec.encrypt(pw));
				output.close();
				System.out.println("Password configured correctly");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error storing encrypted password");
			}
		}
		else
			System.out.println("ERROR: can't find configuration file '"+configFile+"'");
		System.out.println("");
		System.out.println("Press RETURN to continue...");
		s.nextLine();
		s.close();
	}
}
