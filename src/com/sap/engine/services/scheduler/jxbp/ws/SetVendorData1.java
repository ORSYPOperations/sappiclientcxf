
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per setVendorData1 complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="setVendorData1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jobId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setVendorData1", propOrder = {
    "jobId",
    "data"
})
public class SetVendorData1 {

    protected JobID jobId;
    protected String data;

    /**
     * Recupera il valore della proprietÓ jobId.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getJobId() {
        return jobId;
    }

    /**
     * Imposta il valore della proprietÓ jobId.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setJobId(JobID value) {
        this.jobId = value;
    }

    /**
     * Recupera il valore della proprietÓ data.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getData() {
        return data;
    }

    /**
     * Imposta il valore della proprietÓ data.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setData(String value) {
        this.data = value;
    }

}
