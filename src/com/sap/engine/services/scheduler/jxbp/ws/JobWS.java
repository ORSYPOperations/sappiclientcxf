
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per jobWS complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="jobWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CPUTime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cancelRequest" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="id" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *         &lt;element name="jobDefinitionId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobDefinitionID" minOccurs="0"/>
 *         &lt;element name="jobStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="memoryAllocation" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="node" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentJobId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *         &lt;element name="retentionPeriod" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="schedulerId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}schedulerID" minOccurs="0"/>
 *         &lt;element name="schedulerTaskId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}schedulerTaskID" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="submitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jobWS", propOrder = {
    "cpuTime",
    "cancelRequest",
    "endDate",
    "id",
    "jobDefinitionId",
    "jobStatus",
    "memoryAllocation",
    "name",
    "node",
    "parentJobId",
    "retentionPeriod",
    "returnCode",
    "schedulerId",
    "schedulerTaskId",
    "startDate",
    "submitDate",
    "userId",
    "vendorData"
})
public class JobWS {

    @XmlElement(name = "CPUTime")
    protected long cpuTime;
    protected boolean cancelRequest;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    protected JobID id;
    protected JobDefinitionID jobDefinitionId;
    protected String jobStatus;
    protected long memoryAllocation;
    protected String name;
    protected String node;
    protected JobID parentJobId;
    protected int retentionPeriod;
    protected short returnCode;
    protected SchedulerID schedulerId;
    protected SchedulerTaskID schedulerTaskId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar submitDate;
    protected String userId;
    protected String vendorData;

    /**
     * Recupera il valore della proprietÓ cpuTime.
     * 
     */
    public long getCPUTime() {
        return cpuTime;
    }

    /**
     * Imposta il valore della proprietÓ cpuTime.
     * 
     */
    public void setCPUTime(long value) {
        this.cpuTime = value;
    }

    /**
     * Recupera il valore della proprietÓ cancelRequest.
     * 
     */
    public boolean isCancelRequest() {
        return cancelRequest;
    }

    /**
     * Imposta il valore della proprietÓ cancelRequest.
     * 
     */
    public void setCancelRequest(boolean value) {
        this.cancelRequest = value;
    }

    /**
     * Recupera il valore della proprietÓ endDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Imposta il valore della proprietÓ endDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Recupera il valore della proprietÓ id.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getId() {
        return id;
    }

    /**
     * Imposta il valore della proprietÓ id.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setId(JobID value) {
        this.id = value;
    }

    /**
     * Recupera il valore della proprietÓ jobDefinitionId.
     * 
     * @return
     *     possible object is
     *     {@link JobDefinitionID }
     *     
     */
    public JobDefinitionID getJobDefinitionId() {
        return jobDefinitionId;
    }

    /**
     * Imposta il valore della proprietÓ jobDefinitionId.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDefinitionID }
     *     
     */
    public void setJobDefinitionId(JobDefinitionID value) {
        this.jobDefinitionId = value;
    }

    /**
     * Recupera il valore della proprietÓ jobStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobStatus() {
        return jobStatus;
    }

    /**
     * Imposta il valore della proprietÓ jobStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobStatus(String value) {
        this.jobStatus = value;
    }

    /**
     * Recupera il valore della proprietÓ memoryAllocation.
     * 
     */
    public long getMemoryAllocation() {
        return memoryAllocation;
    }

    /**
     * Imposta il valore della proprietÓ memoryAllocation.
     * 
     */
    public void setMemoryAllocation(long value) {
        this.memoryAllocation = value;
    }

    /**
     * Recupera il valore della proprietÓ name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il valore della proprietÓ name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della proprietÓ node.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNode() {
        return node;
    }

    /**
     * Imposta il valore della proprietÓ node.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNode(String value) {
        this.node = value;
    }

    /**
     * Recupera il valore della proprietÓ parentJobId.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getParentJobId() {
        return parentJobId;
    }

    /**
     * Imposta il valore della proprietÓ parentJobId.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setParentJobId(JobID value) {
        this.parentJobId = value;
    }

    /**
     * Recupera il valore della proprietÓ retentionPeriod.
     * 
     */
    public int getRetentionPeriod() {
        return retentionPeriod;
    }

    /**
     * Imposta il valore della proprietÓ retentionPeriod.
     * 
     */
    public void setRetentionPeriod(int value) {
        this.retentionPeriod = value;
    }

    /**
     * Recupera il valore della proprietÓ returnCode.
     * 
     */
    public short getReturnCode() {
        return returnCode;
    }

    /**
     * Imposta il valore della proprietÓ returnCode.
     * 
     */
    public void setReturnCode(short value) {
        this.returnCode = value;
    }

    /**
     * Recupera il valore della proprietÓ schedulerId.
     * 
     * @return
     *     possible object is
     *     {@link SchedulerID }
     *     
     */
    public SchedulerID getSchedulerId() {
        return schedulerId;
    }

    /**
     * Imposta il valore della proprietÓ schedulerId.
     * 
     * @param value
     *     allowed object is
     *     {@link SchedulerID }
     *     
     */
    public void setSchedulerId(SchedulerID value) {
        this.schedulerId = value;
    }

    /**
     * Recupera il valore della proprietÓ schedulerTaskId.
     * 
     * @return
     *     possible object is
     *     {@link SchedulerTaskID }
     *     
     */
    public SchedulerTaskID getSchedulerTaskId() {
        return schedulerTaskId;
    }

    /**
     * Imposta il valore della proprietÓ schedulerTaskId.
     * 
     * @param value
     *     allowed object is
     *     {@link SchedulerTaskID }
     *     
     */
    public void setSchedulerTaskId(SchedulerTaskID value) {
        this.schedulerTaskId = value;
    }

    /**
     * Recupera il valore della proprietÓ startDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Imposta il valore della proprietÓ startDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Recupera il valore della proprietÓ submitDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubmitDate() {
        return submitDate;
    }

    /**
     * Imposta il valore della proprietÓ submitDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubmitDate(XMLGregorianCalendar value) {
        this.submitDate = value;
    }

    /**
     * Recupera il valore della proprietÓ userId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Imposta il valore della proprietÓ userId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Recupera il valore della proprietÓ vendorData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorData() {
        return vendorData;
    }

    /**
     * Imposta il valore della proprietÓ vendorData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorData(String value) {
        this.vendorData = value;
    }

}
