
package com.sap.engine.services.scheduler.jxbp.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per executeJob complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="executeJob">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jobDefId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobDefinitionID" minOccurs="0"/>
 *         &lt;element name="jobParametersWS" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobParameterWS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="retentionPeriod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeJob", propOrder = {
    "jobDefId",
    "jobParametersWS",
    "retentionPeriod"
})
public class ExecuteJob {

    protected JobDefinitionID jobDefId;
    protected List<JobParameterWS> jobParametersWS;
    protected Integer retentionPeriod;

    /**
     * Recupera il valore della proprietÓ jobDefId.
     * 
     * @return
     *     possible object is
     *     {@link JobDefinitionID }
     *     
     */
    public JobDefinitionID getJobDefId() {
        return jobDefId;
    }

    /**
     * Imposta il valore della proprietÓ jobDefId.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDefinitionID }
     *     
     */
    public void setJobDefId(JobDefinitionID value) {
        this.jobDefId = value;
    }

    /**
     * Gets the value of the jobParametersWS property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jobParametersWS property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJobParametersWS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobParameterWS }
     * 
     * 
     */
    public List<JobParameterWS> getJobParametersWS() {
        if (jobParametersWS == null) {
            jobParametersWS = new ArrayList<JobParameterWS>();
        }
        return this.jobParametersWS;
    }

    /**
     * Recupera il valore della proprietÓ retentionPeriod.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRetentionPeriod() {
        return retentionPeriod;
    }

    /**
     * Imposta il valore della proprietÓ retentionPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRetentionPeriod(Integer value) {
        this.retentionPeriod = value;
    }

}
