
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getJobLog complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getJobLog">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jobid" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *         &lt;element name="it" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}logIteratorWS" minOccurs="0"/>
 *         &lt;element name="fetchSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getJobLog", propOrder = {
    "jobid",
    "it",
    "fetchSize"
})
public class GetJobLog {

    protected JobID jobid;
    protected LogIteratorWS it;
    protected int fetchSize;

    /**
     * Recupera il valore della proprietÓ jobid.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getJobid() {
        return jobid;
    }

    /**
     * Imposta il valore della proprietÓ jobid.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setJobid(JobID value) {
        this.jobid = value;
    }

    /**
     * Recupera il valore della proprietÓ it.
     * 
     * @return
     *     possible object is
     *     {@link LogIteratorWS }
     *     
     */
    public LogIteratorWS getIt() {
        return it;
    }

    /**
     * Imposta il valore della proprietÓ it.
     * 
     * @param value
     *     allowed object is
     *     {@link LogIteratorWS }
     *     
     */
    public void setIt(LogIteratorWS value) {
        this.it = value;
    }

    /**
     * Recupera il valore della proprietÓ fetchSize.
     * 
     */
    public int getFetchSize() {
        return fetchSize;
    }

    /**
     * Imposta il valore della proprietÓ fetchSize.
     * 
     */
    public void setFetchSize(int value) {
        this.fetchSize = value;
    }

}
