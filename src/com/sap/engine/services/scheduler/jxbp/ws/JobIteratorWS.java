
package com.sap.engine.services.scheduler.jxbp.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per jobIteratorWS complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="jobIteratorWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jobs" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobWS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="stateDescriptor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jobIteratorWS", propOrder = {
    "jobs",
    "stateDescriptor"
})
public class JobIteratorWS {

    @XmlElement(nillable = true)
    protected List<JobWS> jobs;
    protected String stateDescriptor;

    /**
     * Gets the value of the jobs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jobs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJobs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobWS }
     * 
     * 
     */
    public List<JobWS> getJobs() {
        if (jobs == null) {
            jobs = new ArrayList<JobWS>();
        }
        return this.jobs;
    }

    /**
     * Recupera il valore della proprietÓ stateDescriptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateDescriptor() {
        return stateDescriptor;
    }

    /**
     * Imposta il valore della proprietÓ stateDescriptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateDescriptor(String value) {
        this.stateDescriptor = value;
    }

}
