
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.6
 * 2013-08-23T17:09:07.515+02:00
 * Generated source version: 2.7.6
 */

@WebFault(name = "ParameterValidationException", targetNamespace = "http://sap.com/engine/services/scheduler/jxbp/ws/")
public class JXBPWSexecuteJobFault1 extends Exception {
    
    private com.sap.engine.services.scheduler.jxbp.ws.ParameterValidationException parameterValidationException;

    public JXBPWSexecuteJobFault1() {
        super();
    }
    
    public JXBPWSexecuteJobFault1(String message) {
        super(message);
    }
    
    public JXBPWSexecuteJobFault1(String message, Throwable cause) {
        super(message, cause);
    }

    public JXBPWSexecuteJobFault1(String message, com.sap.engine.services.scheduler.jxbp.ws.ParameterValidationException parameterValidationException) {
        super(message);
        this.parameterValidationException = parameterValidationException;
    }

    public JXBPWSexecuteJobFault1(String message, com.sap.engine.services.scheduler.jxbp.ws.ParameterValidationException parameterValidationException, Throwable cause) {
        super(message, cause);
        this.parameterValidationException = parameterValidationException;
    }

    public com.sap.engine.services.scheduler.jxbp.ws.ParameterValidationException getFaultInfo() {
        return this.parameterValidationException;
    }
}
