
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.6
 * 2013-08-23T17:09:07.594+02:00
 * Generated source version: 2.7.6
 */

@WebFault(name = "JXBPException", targetNamespace = "http://sap.com/engine/services/scheduler/jxbp/ws/")
public class JXBPWScancelJobFault1 extends Exception {
    
    private com.sap.engine.services.scheduler.jxbp.ws.JXBPException jxbpException;

    public JXBPWScancelJobFault1() {
        super();
    }
    
    public JXBPWScancelJobFault1(String message) {
        super(message);
    }
    
    public JXBPWScancelJobFault1(String message, Throwable cause) {
        super(message, cause);
    }

    public JXBPWScancelJobFault1(String message, com.sap.engine.services.scheduler.jxbp.ws.JXBPException jxbpException) {
        super(message);
        this.jxbpException = jxbpException;
    }

    public JXBPWScancelJobFault1(String message, com.sap.engine.services.scheduler.jxbp.ws.JXBPException jxbpException, Throwable cause) {
        super(message, cause);
        this.jxbpException = jxbpException;
    }

    public com.sap.engine.services.scheduler.jxbp.ws.JXBPException getFaultInfo() {
        return this.jxbpException;
    }
}
