
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per eventWS complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="eventWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="additionalParameter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventId" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="parameter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="raisedByDetails" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}abstractIdentifier" minOccurs="0"/>
 *         &lt;element name="raisedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventWS", propOrder = {
    "additionalParameter",
    "eventId",
    "parameter",
    "raisedByDetails",
    "raisedDate",
    "type"
})
public class EventWS {

    protected String additionalParameter;
    protected byte[] eventId;
    protected String parameter;
    protected AbstractIdentifier raisedByDetails;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar raisedDate;
    protected String type;

    /**
     * Recupera il valore della proprietÓ additionalParameter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalParameter() {
        return additionalParameter;
    }

    /**
     * Imposta il valore della proprietÓ additionalParameter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalParameter(String value) {
        this.additionalParameter = value;
    }

    /**
     * Recupera il valore della proprietÓ eventId.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getEventId() {
        return eventId;
    }

    /**
     * Imposta il valore della proprietÓ eventId.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setEventId(byte[] value) {
        this.eventId = value;
    }

    /**
     * Recupera il valore della proprietÓ parameter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * Imposta il valore della proprietÓ parameter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter(String value) {
        this.parameter = value;
    }

    /**
     * Recupera il valore della proprietÓ raisedByDetails.
     * 
     * @return
     *     possible object is
     *     {@link AbstractIdentifier }
     *     
     */
    public AbstractIdentifier getRaisedByDetails() {
        return raisedByDetails;
    }

    /**
     * Imposta il valore della proprietÓ raisedByDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractIdentifier }
     *     
     */
    public void setRaisedByDetails(AbstractIdentifier value) {
        this.raisedByDetails = value;
    }

    /**
     * Recupera il valore della proprietÓ raisedDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRaisedDate() {
        return raisedDate;
    }

    /**
     * Imposta il valore della proprietÓ raisedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRaisedDate(XMLGregorianCalendar value) {
        this.raisedDate = value;
    }

    /**
     * Recupera il valore della proprietÓ type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Imposta il valore della proprietÓ type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
