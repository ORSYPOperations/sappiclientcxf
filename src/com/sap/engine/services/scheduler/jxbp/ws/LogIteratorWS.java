
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per logIteratorWS complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="logIteratorWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nextChunk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pos" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "logIteratorWS", propOrder = {
    "nextChunk",
    "pos"
})
public class LogIteratorWS {

    protected String nextChunk;
    protected long pos;

    /**
     * Recupera il valore della proprietÓ nextChunk.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextChunk() {
        return nextChunk;
    }

    /**
     * Imposta il valore della proprietÓ nextChunk.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextChunk(String value) {
        this.nextChunk = value;
    }

    /**
     * Recupera il valore della proprietÓ pos.
     * 
     */
    public long getPos() {
        return pos;
    }

    /**
     * Imposta il valore della proprietÓ pos.
     * 
     */
    public void setPos(long value) {
        this.pos = value;
    }

}
