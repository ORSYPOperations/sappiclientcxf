
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getJobParameters complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getJobParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jobid" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getJobParameters", propOrder = {
    "jobid"
})
public class GetJobParameters {

    protected JobID jobid;

    /**
     * Recupera il valore della proprietÓ jobid.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getJobid() {
        return jobid;
    }

    /**
     * Imposta il valore della proprietÓ jobid.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setJobid(JobID value) {
        this.jobid = value;
    }

}
