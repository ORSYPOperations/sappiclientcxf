package com.orsyp.integration.sappi.client;

import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.orsyp.integration.sappi.client.util.Codec;
import com.orsyp.integration.sappi.client.util.CustomProperties;
import com.sap.engine.services.scheduler.jxbp.ws.JXBPWS;
import com.sap.engine.services.scheduler.jxbp.ws.JobDefinitionWS;
import com.sap.engine.services.scheduler.jxbp.ws.JobID;
import com.sap.engine.services.scheduler.jxbp.ws.JobParameterDefinitionWS;
import com.sap.engine.services.scheduler.jxbp.ws.JobParameterWS;
import com.sap.engine.services.scheduler.jxbp.ws.JobWS;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;


public class SapPiClient {
	
	/**
	 * @param args
	 * @throws RemoteException 
	 * @throws JXBPException 
	 */
	public static void main(String[] args) throws Exception {
		
		System.out.println("SapPiClient V1.2");
		
		if (args.length!=2) {
			System.out.println("Usage: SapPiClient <connection config file> <sap/pi job name>");
			System.exit(1);
		} 
		
		if (!(new File(args[0])).exists()) {
			System.out.println("Connection config file not found: " + args[0]);
			System.exit(1);
		} 
		
		CustomProperties connProps = new CustomProperties(args[0]);
		String jobName=args[1];
		Map<String, String> env = System.getenv();
		
		//web service connection and authentication
		JaxWsProxyFactoryBean clientFactory = new JaxWsProxyFactoryBean();             
		clientFactory.setAddress(connProps.getProperty("address")); 
		clientFactory.setServiceClass(JXBPWS.class); 
		clientFactory.setUsername(connProps.getProperty("user")); 
		clientFactory.setPassword(Codec.decrypt(connProps.getProperty("password"))); 
		JXBPWS svc = (JXBPWS)clientFactory.create();
		
		//get job
		JobDefinitionWS jd = svc.getJobDefinitionByName(jobName);
		if (jd==null) {
			System.out.println("SAP/PI job not found: " + jobName);
			System.exit(1);			
		}
			
		//set params
		ArrayList<JobParameterWS> params = new ArrayList<JobParameterWS>();
		
		for (JobParameterDefinitionWS def: jd.getParameters()) {
    		String parName = def.getName();    		
    		String val = getParamValue(env,parName);
    		if (val!=null) {
	    		JobParameterWS par = new JobParameterWS();
	    		par.setJobParameterDefinitionWS(def);
	    		
	    		String type = def.getType();
	    		if (type.equalsIgnoreCase("Boolean"))
	    			par.setBooleanValue(Boolean.valueOf(val.equalsIgnoreCase("true")));
	    		else
    			if (type.equalsIgnoreCase("Integer"))
	    			par.setIntegerValue(Integer.parseInt(val));
    			else
    			if (type.equalsIgnoreCase("Double"))
	    			par.setDoubleValue(Double.parseDouble(val));
    			else
    			if (type.equalsIgnoreCase("Float"))
	    			par.setFloatValue(Float.parseFloat(val));
    			else
    			if (type.equalsIgnoreCase("Long"))
	    			par.setLongValue(Long.parseLong(val));
    			else
    			if (type.equalsIgnoreCase("Date")) 
	    			par.setDateValue(XMLGregorianCalendarImpl.parse(val));
    			else
    				par.setStringValue(val);    				
	    				    			 
	    		params.add(par);
	    		if (val.length()==0)
	    			System.out.println("Parameter " + parName + " is empty");
	    		else
	    			System.out.println("Parameter " + parName + "="+val);
    		}
    		else 
    			System.out.println("No matching variable found for parameter " + parName);
		}
		
		System.out.println("Launching job " + jobName + " ...");
		//execute
		JobID jID = svc.executeJob(jd.getId(), params, 100000000);
		System.out.println("Job " + jobName + " executed.");
		
		//check result
		while (true) {
			JobWS j = svc.getJob(jID);
			String status = j.getJobStatus();
			if (status!=null) {
				if (status.equals("ERROR")) {
					System.out.println("Job " + jobName + " ended. Status: " + status);
					System.exit(1);
				}  
				else
				if (status.equals("COMPLETED")) {
					System.out.println("Job " + jobName + " ended. Status: " + status + " - Return code: " + j.getReturnCode());
					System.exit(0);
				}
			}
			Thread.sleep(1000);
		}				    
	}

	private static String getParamValue(Map<String, String> env, String parName) {
		for (Entry<String, String> ent: env.entrySet()) {			
			if (parName.equals(ent.getValue().trim())) {
				String nameKey = ent.getKey().trim();
				String valKey = nameKey + "_VAL";
				String value = env.get(valKey);
				if (value==null)
					return "";
				return value;
			}
		}
		return null;
	}

}
