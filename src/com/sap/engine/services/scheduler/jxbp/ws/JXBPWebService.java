package com.sap.engine.services.scheduler.jxbp.ws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.7.6
 * 2013-08-23T17:09:08.261+02:00
 * Generated source version: 2.7.6
 * 
 */
@WebServiceClient(name = "JXBPWebService", 
                  wsdlLocation = "http://10.2.128.130:8000/scheduler~runtime~gate~web/JXBPWebService?wsdl",
                  targetNamespace = "http://sap.com/engine/services/scheduler/jxbp/ws/") 
public class JXBPWebService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "JXBPWebService");
    public final static QName JXBPWebServicePort = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "JXBPWebServicePort");
    static {
        URL url = null;
        try {
            url = new URL("http://10.2.128.130:8000/scheduler~runtime~gate~web/JXBPWebService?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(JXBPWebService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://10.2.128.130:8000/scheduler~runtime~gate~web/JXBPWebService?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public JXBPWebService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public JXBPWebService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public JXBPWebService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public JXBPWebService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public JXBPWebService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public JXBPWebService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName);
    }

    /**
     *
     * @return
     *     returns JXBPWS
     */
    @WebEndpoint(name = "JXBPWebServicePort")
    public JXBPWS getJXBPWebServicePort() {
        return super.getPort(JXBPWebServicePort, JXBPWS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns JXBPWS
     */
    @WebEndpoint(name = "JXBPWebServicePort")
    public JXBPWS getJXBPWebServicePort(WebServiceFeature... features) {
        return super.getPort(JXBPWebServicePort, JXBPWS.class, features);
    }

}
