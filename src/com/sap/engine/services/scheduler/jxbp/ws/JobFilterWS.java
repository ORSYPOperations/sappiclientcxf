
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per jobFilterWS complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="jobFilterWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="endedTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="jobDefinitionId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobDefinitionID" minOccurs="0"/>
 *         &lt;element name="jobID" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *         &lt;element name="jobStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="node" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentJobId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobID" minOccurs="0"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="schedulerId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}schedulerID" minOccurs="0"/>
 *         &lt;element name="schedulerTaskId" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}schedulerTaskID" minOccurs="0"/>
 *         &lt;element name="startedFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="startedTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jobFilterWS", propOrder = {
    "endedTo",
    "jobDefinitionId",
    "jobID",
    "jobStatus",
    "name",
    "node",
    "parentJobId",
    "returnCode",
    "schedulerId",
    "schedulerTaskId",
    "startedFrom",
    "startedTo",
    "userId",
    "vendorData"
})
public class JobFilterWS {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endedTo;
    protected JobDefinitionID jobDefinitionId;
    protected JobID jobID;
    protected String jobStatus;
    protected String name;
    protected String node;
    protected JobID parentJobId;
    protected Short returnCode;
    protected SchedulerID schedulerId;
    protected SchedulerTaskID schedulerTaskId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startedFrom;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startedTo;
    protected String userId;
    protected String vendorData;

    /**
     * Recupera il valore della proprietÓ endedTo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndedTo() {
        return endedTo;
    }

    /**
     * Imposta il valore della proprietÓ endedTo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndedTo(XMLGregorianCalendar value) {
        this.endedTo = value;
    }

    /**
     * Recupera il valore della proprietÓ jobDefinitionId.
     * 
     * @return
     *     possible object is
     *     {@link JobDefinitionID }
     *     
     */
    public JobDefinitionID getJobDefinitionId() {
        return jobDefinitionId;
    }

    /**
     * Imposta il valore della proprietÓ jobDefinitionId.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDefinitionID }
     *     
     */
    public void setJobDefinitionId(JobDefinitionID value) {
        this.jobDefinitionId = value;
    }

    /**
     * Recupera il valore della proprietÓ jobID.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getJobID() {
        return jobID;
    }

    /**
     * Imposta il valore della proprietÓ jobID.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setJobID(JobID value) {
        this.jobID = value;
    }

    /**
     * Recupera il valore della proprietÓ jobStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobStatus() {
        return jobStatus;
    }

    /**
     * Imposta il valore della proprietÓ jobStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobStatus(String value) {
        this.jobStatus = value;
    }

    /**
     * Recupera il valore della proprietÓ name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il valore della proprietÓ name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della proprietÓ node.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNode() {
        return node;
    }

    /**
     * Imposta il valore della proprietÓ node.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNode(String value) {
        this.node = value;
    }

    /**
     * Recupera il valore della proprietÓ parentJobId.
     * 
     * @return
     *     possible object is
     *     {@link JobID }
     *     
     */
    public JobID getParentJobId() {
        return parentJobId;
    }

    /**
     * Imposta il valore della proprietÓ parentJobId.
     * 
     * @param value
     *     allowed object is
     *     {@link JobID }
     *     
     */
    public void setParentJobId(JobID value) {
        this.parentJobId = value;
    }

    /**
     * Recupera il valore della proprietÓ returnCode.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getReturnCode() {
        return returnCode;
    }

    /**
     * Imposta il valore della proprietÓ returnCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setReturnCode(Short value) {
        this.returnCode = value;
    }

    /**
     * Recupera il valore della proprietÓ schedulerId.
     * 
     * @return
     *     possible object is
     *     {@link SchedulerID }
     *     
     */
    public SchedulerID getSchedulerId() {
        return schedulerId;
    }

    /**
     * Imposta il valore della proprietÓ schedulerId.
     * 
     * @param value
     *     allowed object is
     *     {@link SchedulerID }
     *     
     */
    public void setSchedulerId(SchedulerID value) {
        this.schedulerId = value;
    }

    /**
     * Recupera il valore della proprietÓ schedulerTaskId.
     * 
     * @return
     *     possible object is
     *     {@link SchedulerTaskID }
     *     
     */
    public SchedulerTaskID getSchedulerTaskId() {
        return schedulerTaskId;
    }

    /**
     * Imposta il valore della proprietÓ schedulerTaskId.
     * 
     * @param value
     *     allowed object is
     *     {@link SchedulerTaskID }
     *     
     */
    public void setSchedulerTaskId(SchedulerTaskID value) {
        this.schedulerTaskId = value;
    }

    /**
     * Recupera il valore della proprietÓ startedFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartedFrom() {
        return startedFrom;
    }

    /**
     * Imposta il valore della proprietÓ startedFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartedFrom(XMLGregorianCalendar value) {
        this.startedFrom = value;
    }

    /**
     * Recupera il valore della proprietÓ startedTo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartedTo() {
        return startedTo;
    }

    /**
     * Imposta il valore della proprietÓ startedTo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartedTo(XMLGregorianCalendar value) {
        this.startedTo = value;
    }

    /**
     * Recupera il valore della proprietÓ userId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Imposta il valore della proprietÓ userId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Recupera il valore della proprietÓ vendorData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorData() {
        return vendorData;
    }

    /**
     * Imposta il valore della proprietÓ vendorData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorData(String value) {
        this.vendorData = value;
    }

}
