
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.6
 * 2013-08-23T17:09:07.652+02:00
 * Generated source version: 2.7.6
 */

@WebFault(name = "NoSuchJobException", targetNamespace = "http://sap.com/engine/services/scheduler/jxbp/ws/")
public class JXBPWSgetJobLogFault extends Exception {
    
    private com.sap.engine.services.scheduler.jxbp.ws.NoSuchJobException noSuchJobException;

    public JXBPWSgetJobLogFault() {
        super();
    }
    
    public JXBPWSgetJobLogFault(String message) {
        super(message);
    }
    
    public JXBPWSgetJobLogFault(String message, Throwable cause) {
        super(message, cause);
    }

    public JXBPWSgetJobLogFault(String message, com.sap.engine.services.scheduler.jxbp.ws.NoSuchJobException noSuchJobException) {
        super(message);
        this.noSuchJobException = noSuchJobException;
    }

    public JXBPWSgetJobLogFault(String message, com.sap.engine.services.scheduler.jxbp.ws.NoSuchJobException noSuchJobException, Throwable cause) {
        super(message, cause);
        this.noSuchJobException = noSuchJobException;
    }

    public com.sap.engine.services.scheduler.jxbp.ws.NoSuchJobException getFaultInfo() {
        return this.noSuchJobException;
    }
}
