
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.6
 * 2013-08-23T17:09:07.539+02:00
 * Generated source version: 2.7.6
 */

@WebFault(name = "JXBPException", targetNamespace = "http://sap.com/engine/services/scheduler/jxbp/ws/")
public class ExecuteJobFault extends Exception {
    
    private com.sap.engine.services.scheduler.jxbp.ws.JXBPException jxbpException;

    public ExecuteJobFault() {
        super();
    }
    
    public ExecuteJobFault(String message) {
        super(message);
    }
    
    public ExecuteJobFault(String message, Throwable cause) {
        super(message, cause);
    }

    public ExecuteJobFault(String message, com.sap.engine.services.scheduler.jxbp.ws.JXBPException jxbpException) {
        super(message);
        this.jxbpException = jxbpException;
    }

    public ExecuteJobFault(String message, com.sap.engine.services.scheduler.jxbp.ws.JXBPException jxbpException, Throwable cause) {
        super(message, cause);
        this.jxbpException = jxbpException;
    }

    public com.sap.engine.services.scheduler.jxbp.ws.JXBPException getFaultInfo() {
        return this.jxbpException;
    }
}
