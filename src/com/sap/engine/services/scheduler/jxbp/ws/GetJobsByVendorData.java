
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getJobsByVendorData complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getJobsByVendorData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iterWS" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobIteratorWS" minOccurs="0"/>
 *         &lt;element name="fetchSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getJobsByVendorData", propOrder = {
    "data",
    "iterWS",
    "fetchSize"
})
public class GetJobsByVendorData {

    protected String data;
    protected JobIteratorWS iterWS;
    protected int fetchSize;

    /**
     * Recupera il valore della proprietÓ data.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getData() {
        return data;
    }

    /**
     * Imposta il valore della proprietÓ data.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setData(String value) {
        this.data = value;
    }

    /**
     * Recupera il valore della proprietÓ iterWS.
     * 
     * @return
     *     possible object is
     *     {@link JobIteratorWS }
     *     
     */
    public JobIteratorWS getIterWS() {
        return iterWS;
    }

    /**
     * Imposta il valore della proprietÓ iterWS.
     * 
     * @param value
     *     allowed object is
     *     {@link JobIteratorWS }
     *     
     */
    public void setIterWS(JobIteratorWS value) {
        this.iterWS = value;
    }

    /**
     * Recupera il valore della proprietÓ fetchSize.
     * 
     */
    public int getFetchSize() {
        return fetchSize;
    }

    /**
     * Imposta il valore della proprietÓ fetchSize.
     * 
     */
    public void setFetchSize(int value) {
        this.fetchSize = value;
    }

}
