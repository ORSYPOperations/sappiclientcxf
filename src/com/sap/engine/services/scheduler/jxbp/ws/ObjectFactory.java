
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sap.engine.services.scheduler.jxbp.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExecuteJob1Response_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "executeJob1Response");
    private final static QName _HaveChildJobsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "haveChildJobsResponse");
    private final static QName _SetVendorData1Response_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "setVendorData1Response");
    private final static QName _GetJobDefinitionsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobDefinitionsResponse");
    private final static QName _GetChildJobsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getChildJobsResponse");
    private final static QName _GetVendorData_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getVendorData");
    private final static QName _JXBPException_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "JXBPException");
    private final static QName _SetFilter_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "setFilter");
    private final static QName _GetJobDefinitionById_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobDefinitionById");
    private final static QName _GetJobsByStatus_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobsByStatus");
    private final static QName _CancelJob_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "cancelJob");
    private final static QName _ExecuteJob1_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "executeJob1");
    private final static QName _GetVersion_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getVersion");
    private final static QName _GetJobLog_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobLog");
    private final static QName _HasChildJobs_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "hasChildJobs");
    private final static QName _ExecuteJob_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "executeJob");
    private final static QName _RemoveJobLog_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "removeJobLog");
    private final static QName _GetJobParametersResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobParametersResponse");
    private final static QName _RemoveJobLogResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "removeJobLogResponse");
    private final static QName _HasChildJobsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "hasChildJobsResponse");
    private final static QName _NoSuchJobDefinitionException_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "NoSuchJobDefinitionException");
    private final static QName _GetUnhandledEventsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getUnhandledEventsResponse");
    private final static QName _GetUnhandledEvents_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getUnhandledEvents");
    private final static QName _GetJobDefinitionByName_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobDefinitionByName");
    private final static QName _GetJobs1Response_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobs1Response");
    private final static QName _HaveChildJobs_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "haveChildJobs");
    private final static QName _GetChildJobs_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getChildJobs");
    private final static QName _GetJobsByStatusResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobsByStatusResponse");
    private final static QName _ExecuteJobResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "executeJobResponse");
    private final static QName _RemoveJobsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "removeJobsResponse");
    private final static QName _GetJobs1_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobs1");
    private final static QName _GetJobResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobResponse");
    private final static QName _CancelJobResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "cancelJobResponse");
    private final static QName _SetVendorData1_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "setVendorData1");
    private final static QName _GetJXBPRuntimeEventTypesResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJXBPRuntimeEventTypesResponse");
    private final static QName _ParameterValidationException_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "ParameterValidationException");
    private final static QName _GetVersionResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getVersionResponse");
    private final static QName _GetJXBPRuntimeEventTypes_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJXBPRuntimeEventTypes");
    private final static QName _GetJobsByVendorDataResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobsByVendorDataResponse");
    private final static QName _SetVendorDataResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "setVendorDataResponse");
    private final static QName _JobIllegalStateException_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "JobIllegalStateException");
    private final static QName _SetVendorData_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "setVendorData");
    private final static QName _GetJobStatus_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobStatus");
    private final static QName _RemoveJob_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "removeJob");
    private final static QName _ClearEventsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "clearEventsResponse");
    private final static QName _GetJobDefinitions_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobDefinitions");
    private final static QName _HoldJob_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "holdJob");
    private final static QName _RemoveJobResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "removeJobResponse");
    private final static QName _GetJob_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJob");
    private final static QName _GetJobLogResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobLogResponse");
    private final static QName _NoSuchJobException_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "NoSuchJobException");
    private final static QName _ClearEvents_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "clearEvents");
    private final static QName _ReleaseJob_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "releaseJob");
    private final static QName _GetJobParameters_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobParameters");
    private final static QName _GetJobStatusResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobStatusResponse");
    private final static QName _HoldJobResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "holdJobResponse");
    private final static QName _GetSystemTimeZone_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getSystemTimeZone");
    private final static QName _GetSystemTimeZoneResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getSystemTimeZoneResponse");
    private final static QName _SetFilterResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "setFilterResponse");
    private final static QName _RemoveJobs_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "removeJobs");
    private final static QName _GetJobsByVendorData_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobsByVendorData");
    private final static QName _GetJobsResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobsResponse");
    private final static QName _GetJobs_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobs");
    private final static QName _GetVendorDataResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getVendorDataResponse");
    private final static QName _GetJobDefinitionByIdResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobDefinitionByIdResponse");
    private final static QName _ReleaseJobResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "releaseJobResponse");
    private final static QName _GetJobDefinitionByNameResponse_QNAME = new QName("http://sap.com/engine/services/scheduler/jxbp/ws/", "getJobDefinitionByNameResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sap.engine.services.scheduler.jxbp.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JobDefinitionWS }
     * 
     */
    public JobDefinitionWS createJobDefinitionWS() {
        return new JobDefinitionWS();
    }

    /**
     * Create an instance of {@link JobDefinitionWS.LocalizationInfoMap }
     * 
     */
    public JobDefinitionWS.LocalizationInfoMap createJobDefinitionWSLocalizationInfoMap() {
        return new JobDefinitionWS.LocalizationInfoMap();
    }

    /**
     * Create an instance of {@link JobParameterDefinitionWS }
     * 
     */
    public JobParameterDefinitionWS createJobParameterDefinitionWS() {
        return new JobParameterDefinitionWS();
    }

    /**
     * Create an instance of {@link JobParameterDefinitionWS.LocalizationInfoMap }
     * 
     */
    public JobParameterDefinitionWS.LocalizationInfoMap createJobParameterDefinitionWSLocalizationInfoMap() {
        return new JobParameterDefinitionWS.LocalizationInfoMap();
    }

    /**
     * Create an instance of {@link GetJXBPRuntimeEventTypes }
     * 
     */
    public GetJXBPRuntimeEventTypes createGetJXBPRuntimeEventTypes() {
        return new GetJXBPRuntimeEventTypes();
    }

    /**
     * Create an instance of {@link SetVendorDataResponse }
     * 
     */
    public SetVendorDataResponse createSetVendorDataResponse() {
        return new SetVendorDataResponse();
    }

    /**
     * Create an instance of {@link GetJobsByVendorDataResponse }
     * 
     */
    public GetJobsByVendorDataResponse createGetJobsByVendorDataResponse() {
        return new GetJobsByVendorDataResponse();
    }

    /**
     * Create an instance of {@link GetJXBPRuntimeEventTypesResponse }
     * 
     */
    public GetJXBPRuntimeEventTypesResponse createGetJXBPRuntimeEventTypesResponse() {
        return new GetJXBPRuntimeEventTypesResponse();
    }

    /**
     * Create an instance of {@link ParameterValidationException }
     * 
     */
    public ParameterValidationException createParameterValidationException() {
        return new ParameterValidationException();
    }

    /**
     * Create an instance of {@link GetVersionResponse }
     * 
     */
    public GetVersionResponse createGetVersionResponse() {
        return new GetVersionResponse();
    }

    /**
     * Create an instance of {@link SetVendorData1 }
     * 
     */
    public SetVendorData1 createSetVendorData1() {
        return new SetVendorData1();
    }

    /**
     * Create an instance of {@link GetJob }
     * 
     */
    public GetJob createGetJob() {
        return new GetJob();
    }

    /**
     * Create an instance of {@link GetJobLogResponse }
     * 
     */
    public GetJobLogResponse createGetJobLogResponse() {
        return new GetJobLogResponse();
    }

    /**
     * Create an instance of {@link NoSuchJobException }
     * 
     */
    public NoSuchJobException createNoSuchJobException() {
        return new NoSuchJobException();
    }

    /**
     * Create an instance of {@link SetVendorData }
     * 
     */
    public SetVendorData createSetVendorData() {
        return new SetVendorData();
    }

    /**
     * Create an instance of {@link JobIllegalStateException }
     * 
     */
    public JobIllegalStateException createJobIllegalStateException() {
        return new JobIllegalStateException();
    }

    /**
     * Create an instance of {@link RemoveJob }
     * 
     */
    public RemoveJob createRemoveJob() {
        return new RemoveJob();
    }

    /**
     * Create an instance of {@link GetJobStatus }
     * 
     */
    public GetJobStatus createGetJobStatus() {
        return new GetJobStatus();
    }

    /**
     * Create an instance of {@link RemoveJobResponse }
     * 
     */
    public RemoveJobResponse createRemoveJobResponse() {
        return new RemoveJobResponse();
    }

    /**
     * Create an instance of {@link HoldJob }
     * 
     */
    public HoldJob createHoldJob() {
        return new HoldJob();
    }

    /**
     * Create an instance of {@link GetJobDefinitions }
     * 
     */
    public GetJobDefinitions createGetJobDefinitions() {
        return new GetJobDefinitions();
    }

    /**
     * Create an instance of {@link ClearEventsResponse }
     * 
     */
    public ClearEventsResponse createClearEventsResponse() {
        return new ClearEventsResponse();
    }

    /**
     * Create an instance of {@link HoldJobResponse }
     * 
     */
    public HoldJobResponse createHoldJobResponse() {
        return new HoldJobResponse();
    }

    /**
     * Create an instance of {@link GetSystemTimeZone }
     * 
     */
    public GetSystemTimeZone createGetSystemTimeZone() {
        return new GetSystemTimeZone();
    }

    /**
     * Create an instance of {@link SetFilterResponse }
     * 
     */
    public SetFilterResponse createSetFilterResponse() {
        return new SetFilterResponse();
    }

    /**
     * Create an instance of {@link GetSystemTimeZoneResponse }
     * 
     */
    public GetSystemTimeZoneResponse createGetSystemTimeZoneResponse() {
        return new GetSystemTimeZoneResponse();
    }

    /**
     * Create an instance of {@link ClearEvents }
     * 
     */
    public ClearEvents createClearEvents() {
        return new ClearEvents();
    }

    /**
     * Create an instance of {@link ReleaseJob }
     * 
     */
    public ReleaseJob createReleaseJob() {
        return new ReleaseJob();
    }

    /**
     * Create an instance of {@link GetJobParameters }
     * 
     */
    public GetJobParameters createGetJobParameters() {
        return new GetJobParameters();
    }

    /**
     * Create an instance of {@link GetJobStatusResponse }
     * 
     */
    public GetJobStatusResponse createGetJobStatusResponse() {
        return new GetJobStatusResponse();
    }

    /**
     * Create an instance of {@link GetJobs }
     * 
     */
    public GetJobs createGetJobs() {
        return new GetJobs();
    }

    /**
     * Create an instance of {@link GetVendorDataResponse }
     * 
     */
    public GetVendorDataResponse createGetVendorDataResponse() {
        return new GetVendorDataResponse();
    }

    /**
     * Create an instance of {@link GetJobsResponse }
     * 
     */
    public GetJobsResponse createGetJobsResponse() {
        return new GetJobsResponse();
    }

    /**
     * Create an instance of {@link GetJobDefinitionByIdResponse }
     * 
     */
    public GetJobDefinitionByIdResponse createGetJobDefinitionByIdResponse() {
        return new GetJobDefinitionByIdResponse();
    }

    /**
     * Create an instance of {@link ReleaseJobResponse }
     * 
     */
    public ReleaseJobResponse createReleaseJobResponse() {
        return new ReleaseJobResponse();
    }

    /**
     * Create an instance of {@link GetJobDefinitionByNameResponse }
     * 
     */
    public GetJobDefinitionByNameResponse createGetJobDefinitionByNameResponse() {
        return new GetJobDefinitionByNameResponse();
    }

    /**
     * Create an instance of {@link RemoveJobs }
     * 
     */
    public RemoveJobs createRemoveJobs() {
        return new RemoveJobs();
    }

    /**
     * Create an instance of {@link GetJobsByVendorData }
     * 
     */
    public GetJobsByVendorData createGetJobsByVendorData() {
        return new GetJobsByVendorData();
    }

    /**
     * Create an instance of {@link ExecuteJob1Response }
     * 
     */
    public ExecuteJob1Response createExecuteJob1Response() {
        return new ExecuteJob1Response();
    }

    /**
     * Create an instance of {@link SetVendorData1Response }
     * 
     */
    public SetVendorData1Response createSetVendorData1Response() {
        return new SetVendorData1Response();
    }

    /**
     * Create an instance of {@link HaveChildJobsResponse }
     * 
     */
    public HaveChildJobsResponse createHaveChildJobsResponse() {
        return new HaveChildJobsResponse();
    }

    /**
     * Create an instance of {@link CancelJob }
     * 
     */
    public CancelJob createCancelJob() {
        return new CancelJob();
    }

    /**
     * Create an instance of {@link ExecuteJob1 }
     * 
     */
    public ExecuteJob1 createExecuteJob1() {
        return new ExecuteJob1();
    }

    /**
     * Create an instance of {@link GetVersion }
     * 
     */
    public GetVersion createGetVersion() {
        return new GetVersion();
    }

    /**
     * Create an instance of {@link GetJobLog }
     * 
     */
    public GetJobLog createGetJobLog() {
        return new GetJobLog();
    }

    /**
     * Create an instance of {@link GetJobDefinitionsResponse }
     * 
     */
    public GetJobDefinitionsResponse createGetJobDefinitionsResponse() {
        return new GetJobDefinitionsResponse();
    }

    /**
     * Create an instance of {@link JXBPException }
     * 
     */
    public JXBPException createJXBPException() {
        return new JXBPException();
    }

    /**
     * Create an instance of {@link GetChildJobsResponse }
     * 
     */
    public GetChildJobsResponse createGetChildJobsResponse() {
        return new GetChildJobsResponse();
    }

    /**
     * Create an instance of {@link GetVendorData }
     * 
     */
    public GetVendorData createGetVendorData() {
        return new GetVendorData();
    }

    /**
     * Create an instance of {@link SetFilter }
     * 
     */
    public SetFilter createSetFilter() {
        return new SetFilter();
    }

    /**
     * Create an instance of {@link GetJobDefinitionById }
     * 
     */
    public GetJobDefinitionById createGetJobDefinitionById() {
        return new GetJobDefinitionById();
    }

    /**
     * Create an instance of {@link GetJobsByStatus }
     * 
     */
    public GetJobsByStatus createGetJobsByStatus() {
        return new GetJobsByStatus();
    }

    /**
     * Create an instance of {@link NoSuchJobDefinitionException }
     * 
     */
    public NoSuchJobDefinitionException createNoSuchJobDefinitionException() {
        return new NoSuchJobDefinitionException();
    }

    /**
     * Create an instance of {@link GetUnhandledEvents }
     * 
     */
    public GetUnhandledEvents createGetUnhandledEvents() {
        return new GetUnhandledEvents();
    }

    /**
     * Create an instance of {@link GetUnhandledEventsResponse }
     * 
     */
    public GetUnhandledEventsResponse createGetUnhandledEventsResponse() {
        return new GetUnhandledEventsResponse();
    }

    /**
     * Create an instance of {@link GetJobDefinitionByName }
     * 
     */
    public GetJobDefinitionByName createGetJobDefinitionByName() {
        return new GetJobDefinitionByName();
    }

    /**
     * Create an instance of {@link GetJobs1Response }
     * 
     */
    public GetJobs1Response createGetJobs1Response() {
        return new GetJobs1Response();
    }

    /**
     * Create an instance of {@link RemoveJobLog }
     * 
     */
    public RemoveJobLog createRemoveJobLog() {
        return new RemoveJobLog();
    }

    /**
     * Create an instance of {@link ExecuteJob }
     * 
     */
    public ExecuteJob createExecuteJob() {
        return new ExecuteJob();
    }

    /**
     * Create an instance of {@link HasChildJobs }
     * 
     */
    public HasChildJobs createHasChildJobs() {
        return new HasChildJobs();
    }

    /**
     * Create an instance of {@link RemoveJobLogResponse }
     * 
     */
    public RemoveJobLogResponse createRemoveJobLogResponse() {
        return new RemoveJobLogResponse();
    }

    /**
     * Create an instance of {@link GetJobParametersResponse }
     * 
     */
    public GetJobParametersResponse createGetJobParametersResponse() {
        return new GetJobParametersResponse();
    }

    /**
     * Create an instance of {@link HasChildJobsResponse }
     * 
     */
    public HasChildJobsResponse createHasChildJobsResponse() {
        return new HasChildJobsResponse();
    }

    /**
     * Create an instance of {@link RemoveJobsResponse }
     * 
     */
    public RemoveJobsResponse createRemoveJobsResponse() {
        return new RemoveJobsResponse();
    }

    /**
     * Create an instance of {@link GetJobs1 }
     * 
     */
    public GetJobs1 createGetJobs1() {
        return new GetJobs1();
    }

    /**
     * Create an instance of {@link CancelJobResponse }
     * 
     */
    public CancelJobResponse createCancelJobResponse() {
        return new CancelJobResponse();
    }

    /**
     * Create an instance of {@link GetJobResponse }
     * 
     */
    public GetJobResponse createGetJobResponse() {
        return new GetJobResponse();
    }

    /**
     * Create an instance of {@link GetChildJobs }
     * 
     */
    public GetChildJobs createGetChildJobs() {
        return new GetChildJobs();
    }

    /**
     * Create an instance of {@link HaveChildJobs }
     * 
     */
    public HaveChildJobs createHaveChildJobs() {
        return new HaveChildJobs();
    }

    /**
     * Create an instance of {@link ExecuteJobResponse }
     * 
     */
    public ExecuteJobResponse createExecuteJobResponse() {
        return new ExecuteJobResponse();
    }

    /**
     * Create an instance of {@link GetJobsByStatusResponse }
     * 
     */
    public GetJobsByStatusResponse createGetJobsByStatusResponse() {
        return new GetJobsByStatusResponse();
    }

    /**
     * Create an instance of {@link EventWS }
     * 
     */
    public EventWS createEventWS() {
        return new EventWS();
    }

    /**
     * Create an instance of {@link HashMap }
     * 
     */
    public HashMap createHashMap() {
        return new HashMap();
    }

    /**
     * Create an instance of {@link JobWS }
     * 
     */
    public JobWS createJobWS() {
        return new JobWS();
    }

    /**
     * Create an instance of {@link SchedulerID }
     * 
     */
    public SchedulerID createSchedulerID() {
        return new SchedulerID();
    }

    /**
     * Create an instance of {@link SchedulerTaskID }
     * 
     */
    public SchedulerTaskID createSchedulerTaskID() {
        return new SchedulerTaskID();
    }

    /**
     * Create an instance of {@link JobDefinitionID }
     * 
     */
    public JobDefinitionID createJobDefinitionID() {
        return new JobDefinitionID();
    }

    /**
     * Create an instance of {@link JobParameterWS }
     * 
     */
    public JobParameterWS createJobParameterWS() {
        return new JobParameterWS();
    }

    /**
     * Create an instance of {@link LogIteratorWS }
     * 
     */
    public LogIteratorWS createLogIteratorWS() {
        return new LogIteratorWS();
    }

    /**
     * Create an instance of {@link JobFilterWS }
     * 
     */
    public JobFilterWS createJobFilterWS() {
        return new JobFilterWS();
    }

    /**
     * Create an instance of {@link JobID }
     * 
     */
    public JobID createJobID() {
        return new JobID();
    }

    /**
     * Create an instance of {@link JobIteratorWS }
     * 
     */
    public JobIteratorWS createJobIteratorWS() {
        return new JobIteratorWS();
    }

    /**
     * Create an instance of {@link JobDefinitionWS.LocalizationInfoMap.Entry }
     * 
     */
    public JobDefinitionWS.LocalizationInfoMap.Entry createJobDefinitionWSLocalizationInfoMapEntry() {
        return new JobDefinitionWS.LocalizationInfoMap.Entry();
    }

    /**
     * Create an instance of {@link JobParameterDefinitionWS.LocalizationInfoMap.Entry }
     * 
     */
    public JobParameterDefinitionWS.LocalizationInfoMap.Entry createJobParameterDefinitionWSLocalizationInfoMapEntry() {
        return new JobParameterDefinitionWS.LocalizationInfoMap.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteJob1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "executeJob1Response")
    public JAXBElement<ExecuteJob1Response> createExecuteJob1Response(ExecuteJob1Response value) {
        return new JAXBElement<ExecuteJob1Response>(_ExecuteJob1Response_QNAME, ExecuteJob1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaveChildJobsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "haveChildJobsResponse")
    public JAXBElement<HaveChildJobsResponse> createHaveChildJobsResponse(HaveChildJobsResponse value) {
        return new JAXBElement<HaveChildJobsResponse>(_HaveChildJobsResponse_QNAME, HaveChildJobsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetVendorData1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "setVendorData1Response")
    public JAXBElement<SetVendorData1Response> createSetVendorData1Response(SetVendorData1Response value) {
        return new JAXBElement<SetVendorData1Response>(_SetVendorData1Response_QNAME, SetVendorData1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobDefinitionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobDefinitionsResponse")
    public JAXBElement<GetJobDefinitionsResponse> createGetJobDefinitionsResponse(GetJobDefinitionsResponse value) {
        return new JAXBElement<GetJobDefinitionsResponse>(_GetJobDefinitionsResponse_QNAME, GetJobDefinitionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChildJobsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getChildJobsResponse")
    public JAXBElement<GetChildJobsResponse> createGetChildJobsResponse(GetChildJobsResponse value) {
        return new JAXBElement<GetChildJobsResponse>(_GetChildJobsResponse_QNAME, GetChildJobsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVendorData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getVendorData")
    public JAXBElement<GetVendorData> createGetVendorData(GetVendorData value) {
        return new JAXBElement<GetVendorData>(_GetVendorData_QNAME, GetVendorData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JXBPException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "JXBPException")
    public JAXBElement<JXBPException> createJXBPException(JXBPException value) {
        return new JAXBElement<JXBPException>(_JXBPException_QNAME, JXBPException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "setFilter")
    public JAXBElement<SetFilter> createSetFilter(SetFilter value) {
        return new JAXBElement<SetFilter>(_SetFilter_QNAME, SetFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobDefinitionById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobDefinitionById")
    public JAXBElement<GetJobDefinitionById> createGetJobDefinitionById(GetJobDefinitionById value) {
        return new JAXBElement<GetJobDefinitionById>(_GetJobDefinitionById_QNAME, GetJobDefinitionById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobsByStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobsByStatus")
    public JAXBElement<GetJobsByStatus> createGetJobsByStatus(GetJobsByStatus value) {
        return new JAXBElement<GetJobsByStatus>(_GetJobsByStatus_QNAME, GetJobsByStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelJob }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "cancelJob")
    public JAXBElement<CancelJob> createCancelJob(CancelJob value) {
        return new JAXBElement<CancelJob>(_CancelJob_QNAME, CancelJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteJob1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "executeJob1")
    public JAXBElement<ExecuteJob1> createExecuteJob1(ExecuteJob1 value) {
        return new JAXBElement<ExecuteJob1>(_ExecuteJob1_QNAME, ExecuteJob1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVersion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getVersion")
    public JAXBElement<GetVersion> createGetVersion(GetVersion value) {
        return new JAXBElement<GetVersion>(_GetVersion_QNAME, GetVersion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobLog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobLog")
    public JAXBElement<GetJobLog> createGetJobLog(GetJobLog value) {
        return new JAXBElement<GetJobLog>(_GetJobLog_QNAME, GetJobLog.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HasChildJobs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "hasChildJobs")
    public JAXBElement<HasChildJobs> createHasChildJobs(HasChildJobs value) {
        return new JAXBElement<HasChildJobs>(_HasChildJobs_QNAME, HasChildJobs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteJob }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "executeJob")
    public JAXBElement<ExecuteJob> createExecuteJob(ExecuteJob value) {
        return new JAXBElement<ExecuteJob>(_ExecuteJob_QNAME, ExecuteJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJobLog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "removeJobLog")
    public JAXBElement<RemoveJobLog> createRemoveJobLog(RemoveJobLog value) {
        return new JAXBElement<RemoveJobLog>(_RemoveJobLog_QNAME, RemoveJobLog.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobParametersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobParametersResponse")
    public JAXBElement<GetJobParametersResponse> createGetJobParametersResponse(GetJobParametersResponse value) {
        return new JAXBElement<GetJobParametersResponse>(_GetJobParametersResponse_QNAME, GetJobParametersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJobLogResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "removeJobLogResponse")
    public JAXBElement<RemoveJobLogResponse> createRemoveJobLogResponse(RemoveJobLogResponse value) {
        return new JAXBElement<RemoveJobLogResponse>(_RemoveJobLogResponse_QNAME, RemoveJobLogResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HasChildJobsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "hasChildJobsResponse")
    public JAXBElement<HasChildJobsResponse> createHasChildJobsResponse(HasChildJobsResponse value) {
        return new JAXBElement<HasChildJobsResponse>(_HasChildJobsResponse_QNAME, HasChildJobsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSuchJobDefinitionException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "NoSuchJobDefinitionException")
    public JAXBElement<NoSuchJobDefinitionException> createNoSuchJobDefinitionException(NoSuchJobDefinitionException value) {
        return new JAXBElement<NoSuchJobDefinitionException>(_NoSuchJobDefinitionException_QNAME, NoSuchJobDefinitionException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnhandledEventsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getUnhandledEventsResponse")
    public JAXBElement<GetUnhandledEventsResponse> createGetUnhandledEventsResponse(GetUnhandledEventsResponse value) {
        return new JAXBElement<GetUnhandledEventsResponse>(_GetUnhandledEventsResponse_QNAME, GetUnhandledEventsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnhandledEvents }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getUnhandledEvents")
    public JAXBElement<GetUnhandledEvents> createGetUnhandledEvents(GetUnhandledEvents value) {
        return new JAXBElement<GetUnhandledEvents>(_GetUnhandledEvents_QNAME, GetUnhandledEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobDefinitionByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobDefinitionByName")
    public JAXBElement<GetJobDefinitionByName> createGetJobDefinitionByName(GetJobDefinitionByName value) {
        return new JAXBElement<GetJobDefinitionByName>(_GetJobDefinitionByName_QNAME, GetJobDefinitionByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobs1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobs1Response")
    public JAXBElement<GetJobs1Response> createGetJobs1Response(GetJobs1Response value) {
        return new JAXBElement<GetJobs1Response>(_GetJobs1Response_QNAME, GetJobs1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HaveChildJobs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "haveChildJobs")
    public JAXBElement<HaveChildJobs> createHaveChildJobs(HaveChildJobs value) {
        return new JAXBElement<HaveChildJobs>(_HaveChildJobs_QNAME, HaveChildJobs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChildJobs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getChildJobs")
    public JAXBElement<GetChildJobs> createGetChildJobs(GetChildJobs value) {
        return new JAXBElement<GetChildJobs>(_GetChildJobs_QNAME, GetChildJobs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobsByStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobsByStatusResponse")
    public JAXBElement<GetJobsByStatusResponse> createGetJobsByStatusResponse(GetJobsByStatusResponse value) {
        return new JAXBElement<GetJobsByStatusResponse>(_GetJobsByStatusResponse_QNAME, GetJobsByStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteJobResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "executeJobResponse")
    public JAXBElement<ExecuteJobResponse> createExecuteJobResponse(ExecuteJobResponse value) {
        return new JAXBElement<ExecuteJobResponse>(_ExecuteJobResponse_QNAME, ExecuteJobResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJobsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "removeJobsResponse")
    public JAXBElement<RemoveJobsResponse> createRemoveJobsResponse(RemoveJobsResponse value) {
        return new JAXBElement<RemoveJobsResponse>(_RemoveJobsResponse_QNAME, RemoveJobsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobs1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobs1")
    public JAXBElement<GetJobs1> createGetJobs1(GetJobs1 value) {
        return new JAXBElement<GetJobs1>(_GetJobs1_QNAME, GetJobs1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobResponse")
    public JAXBElement<GetJobResponse> createGetJobResponse(GetJobResponse value) {
        return new JAXBElement<GetJobResponse>(_GetJobResponse_QNAME, GetJobResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelJobResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "cancelJobResponse")
    public JAXBElement<CancelJobResponse> createCancelJobResponse(CancelJobResponse value) {
        return new JAXBElement<CancelJobResponse>(_CancelJobResponse_QNAME, CancelJobResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetVendorData1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "setVendorData1")
    public JAXBElement<SetVendorData1> createSetVendorData1(SetVendorData1 value) {
        return new JAXBElement<SetVendorData1>(_SetVendorData1_QNAME, SetVendorData1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJXBPRuntimeEventTypesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJXBPRuntimeEventTypesResponse")
    public JAXBElement<GetJXBPRuntimeEventTypesResponse> createGetJXBPRuntimeEventTypesResponse(GetJXBPRuntimeEventTypesResponse value) {
        return new JAXBElement<GetJXBPRuntimeEventTypesResponse>(_GetJXBPRuntimeEventTypesResponse_QNAME, GetJXBPRuntimeEventTypesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParameterValidationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "ParameterValidationException")
    public JAXBElement<ParameterValidationException> createParameterValidationException(ParameterValidationException value) {
        return new JAXBElement<ParameterValidationException>(_ParameterValidationException_QNAME, ParameterValidationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVersionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getVersionResponse")
    public JAXBElement<GetVersionResponse> createGetVersionResponse(GetVersionResponse value) {
        return new JAXBElement<GetVersionResponse>(_GetVersionResponse_QNAME, GetVersionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJXBPRuntimeEventTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJXBPRuntimeEventTypes")
    public JAXBElement<GetJXBPRuntimeEventTypes> createGetJXBPRuntimeEventTypes(GetJXBPRuntimeEventTypes value) {
        return new JAXBElement<GetJXBPRuntimeEventTypes>(_GetJXBPRuntimeEventTypes_QNAME, GetJXBPRuntimeEventTypes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobsByVendorDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobsByVendorDataResponse")
    public JAXBElement<GetJobsByVendorDataResponse> createGetJobsByVendorDataResponse(GetJobsByVendorDataResponse value) {
        return new JAXBElement<GetJobsByVendorDataResponse>(_GetJobsByVendorDataResponse_QNAME, GetJobsByVendorDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetVendorDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "setVendorDataResponse")
    public JAXBElement<SetVendorDataResponse> createSetVendorDataResponse(SetVendorDataResponse value) {
        return new JAXBElement<SetVendorDataResponse>(_SetVendorDataResponse_QNAME, SetVendorDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobIllegalStateException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "JobIllegalStateException")
    public JAXBElement<JobIllegalStateException> createJobIllegalStateException(JobIllegalStateException value) {
        return new JAXBElement<JobIllegalStateException>(_JobIllegalStateException_QNAME, JobIllegalStateException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetVendorData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "setVendorData")
    public JAXBElement<SetVendorData> createSetVendorData(SetVendorData value) {
        return new JAXBElement<SetVendorData>(_SetVendorData_QNAME, SetVendorData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobStatus")
    public JAXBElement<GetJobStatus> createGetJobStatus(GetJobStatus value) {
        return new JAXBElement<GetJobStatus>(_GetJobStatus_QNAME, GetJobStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJob }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "removeJob")
    public JAXBElement<RemoveJob> createRemoveJob(RemoveJob value) {
        return new JAXBElement<RemoveJob>(_RemoveJob_QNAME, RemoveJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearEventsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "clearEventsResponse")
    public JAXBElement<ClearEventsResponse> createClearEventsResponse(ClearEventsResponse value) {
        return new JAXBElement<ClearEventsResponse>(_ClearEventsResponse_QNAME, ClearEventsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobDefinitions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobDefinitions")
    public JAXBElement<GetJobDefinitions> createGetJobDefinitions(GetJobDefinitions value) {
        return new JAXBElement<GetJobDefinitions>(_GetJobDefinitions_QNAME, GetJobDefinitions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HoldJob }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "holdJob")
    public JAXBElement<HoldJob> createHoldJob(HoldJob value) {
        return new JAXBElement<HoldJob>(_HoldJob_QNAME, HoldJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJobResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "removeJobResponse")
    public JAXBElement<RemoveJobResponse> createRemoveJobResponse(RemoveJobResponse value) {
        return new JAXBElement<RemoveJobResponse>(_RemoveJobResponse_QNAME, RemoveJobResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJob }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJob")
    public JAXBElement<GetJob> createGetJob(GetJob value) {
        return new JAXBElement<GetJob>(_GetJob_QNAME, GetJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobLogResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobLogResponse")
    public JAXBElement<GetJobLogResponse> createGetJobLogResponse(GetJobLogResponse value) {
        return new JAXBElement<GetJobLogResponse>(_GetJobLogResponse_QNAME, GetJobLogResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSuchJobException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "NoSuchJobException")
    public JAXBElement<NoSuchJobException> createNoSuchJobException(NoSuchJobException value) {
        return new JAXBElement<NoSuchJobException>(_NoSuchJobException_QNAME, NoSuchJobException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearEvents }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "clearEvents")
    public JAXBElement<ClearEvents> createClearEvents(ClearEvents value) {
        return new JAXBElement<ClearEvents>(_ClearEvents_QNAME, ClearEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReleaseJob }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "releaseJob")
    public JAXBElement<ReleaseJob> createReleaseJob(ReleaseJob value) {
        return new JAXBElement<ReleaseJob>(_ReleaseJob_QNAME, ReleaseJob.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobParameters")
    public JAXBElement<GetJobParameters> createGetJobParameters(GetJobParameters value) {
        return new JAXBElement<GetJobParameters>(_GetJobParameters_QNAME, GetJobParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobStatusResponse")
    public JAXBElement<GetJobStatusResponse> createGetJobStatusResponse(GetJobStatusResponse value) {
        return new JAXBElement<GetJobStatusResponse>(_GetJobStatusResponse_QNAME, GetJobStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HoldJobResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "holdJobResponse")
    public JAXBElement<HoldJobResponse> createHoldJobResponse(HoldJobResponse value) {
        return new JAXBElement<HoldJobResponse>(_HoldJobResponse_QNAME, HoldJobResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSystemTimeZone }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getSystemTimeZone")
    public JAXBElement<GetSystemTimeZone> createGetSystemTimeZone(GetSystemTimeZone value) {
        return new JAXBElement<GetSystemTimeZone>(_GetSystemTimeZone_QNAME, GetSystemTimeZone.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSystemTimeZoneResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getSystemTimeZoneResponse")
    public JAXBElement<GetSystemTimeZoneResponse> createGetSystemTimeZoneResponse(GetSystemTimeZoneResponse value) {
        return new JAXBElement<GetSystemTimeZoneResponse>(_GetSystemTimeZoneResponse_QNAME, GetSystemTimeZoneResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFilterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "setFilterResponse")
    public JAXBElement<SetFilterResponse> createSetFilterResponse(SetFilterResponse value) {
        return new JAXBElement<SetFilterResponse>(_SetFilterResponse_QNAME, SetFilterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveJobs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "removeJobs")
    public JAXBElement<RemoveJobs> createRemoveJobs(RemoveJobs value) {
        return new JAXBElement<RemoveJobs>(_RemoveJobs_QNAME, RemoveJobs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobsByVendorData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobsByVendorData")
    public JAXBElement<GetJobsByVendorData> createGetJobsByVendorData(GetJobsByVendorData value) {
        return new JAXBElement<GetJobsByVendorData>(_GetJobsByVendorData_QNAME, GetJobsByVendorData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobsResponse")
    public JAXBElement<GetJobsResponse> createGetJobsResponse(GetJobsResponse value) {
        return new JAXBElement<GetJobsResponse>(_GetJobsResponse_QNAME, GetJobsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobs")
    public JAXBElement<GetJobs> createGetJobs(GetJobs value) {
        return new JAXBElement<GetJobs>(_GetJobs_QNAME, GetJobs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVendorDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getVendorDataResponse")
    public JAXBElement<GetVendorDataResponse> createGetVendorDataResponse(GetVendorDataResponse value) {
        return new JAXBElement<GetVendorDataResponse>(_GetVendorDataResponse_QNAME, GetVendorDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobDefinitionByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobDefinitionByIdResponse")
    public JAXBElement<GetJobDefinitionByIdResponse> createGetJobDefinitionByIdResponse(GetJobDefinitionByIdResponse value) {
        return new JAXBElement<GetJobDefinitionByIdResponse>(_GetJobDefinitionByIdResponse_QNAME, GetJobDefinitionByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReleaseJobResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "releaseJobResponse")
    public JAXBElement<ReleaseJobResponse> createReleaseJobResponse(ReleaseJobResponse value) {
        return new JAXBElement<ReleaseJobResponse>(_ReleaseJobResponse_QNAME, ReleaseJobResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobDefinitionByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/engine/services/scheduler/jxbp/ws/", name = "getJobDefinitionByNameResponse")
    public JAXBElement<GetJobDefinitionByNameResponse> createGetJobDefinitionByNameResponse(GetJobDefinitionByNameResponse value) {
        return new JAXBElement<GetJobDefinitionByNameResponse>(_GetJobDefinitionByNameResponse_QNAME, GetJobDefinitionByNameResponse.class, null, value);
    }

}
