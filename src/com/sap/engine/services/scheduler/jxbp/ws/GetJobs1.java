
package com.sap.engine.services.scheduler.jxbp.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getJobs1 complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getJobs1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filterWS" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobFilterWS" minOccurs="0"/>
 *         &lt;element name="iterWS" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobIteratorWS" minOccurs="0"/>
 *         &lt;element name="fetchSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getJobs1", propOrder = {
    "filterWS",
    "iterWS",
    "fetchSize"
})
public class GetJobs1 {

    protected JobFilterWS filterWS;
    protected JobIteratorWS iterWS;
    protected int fetchSize;

    /**
     * Recupera il valore della proprietÓ filterWS.
     * 
     * @return
     *     possible object is
     *     {@link JobFilterWS }
     *     
     */
    public JobFilterWS getFilterWS() {
        return filterWS;
    }

    /**
     * Imposta il valore della proprietÓ filterWS.
     * 
     * @param value
     *     allowed object is
     *     {@link JobFilterWS }
     *     
     */
    public void setFilterWS(JobFilterWS value) {
        this.filterWS = value;
    }

    /**
     * Recupera il valore della proprietÓ iterWS.
     * 
     * @return
     *     possible object is
     *     {@link JobIteratorWS }
     *     
     */
    public JobIteratorWS getIterWS() {
        return iterWS;
    }

    /**
     * Imposta il valore della proprietÓ iterWS.
     * 
     * @param value
     *     allowed object is
     *     {@link JobIteratorWS }
     *     
     */
    public void setIterWS(JobIteratorWS value) {
        this.iterWS = value;
    }

    /**
     * Recupera il valore della proprietÓ fetchSize.
     * 
     */
    public int getFetchSize() {
        return fetchSize;
    }

    /**
     * Imposta il valore della proprietÓ fetchSize.
     * 
     */
    public void setFetchSize(int value) {
        this.fetchSize = value;
    }

}
