
package com.sap.engine.services.scheduler.jxbp.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.java.dev.jaxb.array.StringArray;


/**
 * <p>Classe Java per jobDefinitionWS complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="jobDefinitionWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="applicationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobDefinitionID" minOccurs="0"/>
 *         &lt;element name="jobName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="localizationInfoMap">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="value" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}hashMap" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="parameters" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}jobParameterDefinitionWS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="properties" type="{http://jaxb.dev.java.net/array}stringArray" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="removeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="retentionPeriod" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jobDefinitionWS", propOrder = {
    "applicationName",
    "description",
    "id",
    "jobName",
    "localizationInfoMap",
    "parameters",
    "properties",
    "removeDate",
    "retentionPeriod",
    "type"
})
public class JobDefinitionWS {

    protected String applicationName;
    protected String description;
    protected JobDefinitionID id;
    protected String jobName;
    @XmlElement(required = true)
    protected JobDefinitionWS.LocalizationInfoMap localizationInfoMap;
    @XmlElement(nillable = true)
    protected List<JobParameterDefinitionWS> parameters;
    @XmlElement(nillable = true)
    protected List<StringArray> properties;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar removeDate;
    protected int retentionPeriod;
    protected int type;

    /**
     * Recupera il valore della proprietÓ applicationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Imposta il valore della proprietÓ applicationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationName(String value) {
        this.applicationName = value;
    }

    /**
     * Recupera il valore della proprietÓ description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Imposta il valore della proprietÓ description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Recupera il valore della proprietÓ id.
     * 
     * @return
     *     possible object is
     *     {@link JobDefinitionID }
     *     
     */
    public JobDefinitionID getId() {
        return id;
    }

    /**
     * Imposta il valore della proprietÓ id.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDefinitionID }
     *     
     */
    public void setId(JobDefinitionID value) {
        this.id = value;
    }

    /**
     * Recupera il valore della proprietÓ jobName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Imposta il valore della proprietÓ jobName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobName(String value) {
        this.jobName = value;
    }

    /**
     * Recupera il valore della proprietÓ localizationInfoMap.
     * 
     * @return
     *     possible object is
     *     {@link JobDefinitionWS.LocalizationInfoMap }
     *     
     */
    public JobDefinitionWS.LocalizationInfoMap getLocalizationInfoMap() {
        return localizationInfoMap;
    }

    /**
     * Imposta il valore della proprietÓ localizationInfoMap.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDefinitionWS.LocalizationInfoMap }
     *     
     */
    public void setLocalizationInfoMap(JobDefinitionWS.LocalizationInfoMap value) {
        this.localizationInfoMap = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobParameterDefinitionWS }
     * 
     * 
     */
    public List<JobParameterDefinitionWS> getParameters() {
        if (parameters == null) {
            parameters = new ArrayList<JobParameterDefinitionWS>();
        }
        return this.parameters;
    }

    /**
     * Gets the value of the properties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the properties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StringArray }
     * 
     * 
     */
    public List<StringArray> getProperties() {
        if (properties == null) {
            properties = new ArrayList<StringArray>();
        }
        return this.properties;
    }

    /**
     * Recupera il valore della proprietÓ removeDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRemoveDate() {
        return removeDate;
    }

    /**
     * Imposta il valore della proprietÓ removeDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRemoveDate(XMLGregorianCalendar value) {
        this.removeDate = value;
    }

    /**
     * Recupera il valore della proprietÓ retentionPeriod.
     * 
     */
    public int getRetentionPeriod() {
        return retentionPeriod;
    }

    /**
     * Imposta il valore della proprietÓ retentionPeriod.
     * 
     */
    public void setRetentionPeriod(int value) {
        this.retentionPeriod = value;
    }

    /**
     * Recupera il valore della proprietÓ type.
     * 
     */
    public int getType() {
        return type;
    }

    /**
     * Imposta il valore della proprietÓ type.
     * 
     */
    public void setType(int value) {
        this.type = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="value" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}hashMap" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class LocalizationInfoMap {

        protected List<JobDefinitionWS.LocalizationInfoMap.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JobDefinitionWS.LocalizationInfoMap.Entry }
         * 
         * 
         */
        public List<JobDefinitionWS.LocalizationInfoMap.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<JobDefinitionWS.LocalizationInfoMap.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="value" type="{http://sap.com/engine/services/scheduler/jxbp/ws/}hashMap" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "value"
        })
        public static class Entry {

            protected String key;
            protected HashMap value;

            /**
             * Recupera il valore della proprietÓ key.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKey() {
                return key;
            }

            /**
             * Imposta il valore della proprietÓ key.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKey(String value) {
                this.key = value;
            }

            /**
             * Recupera il valore della proprietÓ value.
             * 
             * @return
             *     possible object is
             *     {@link HashMap }
             *     
             */
            public HashMap getValue() {
                return value;
            }

            /**
             * Imposta il valore della proprietÓ value.
             * 
             * @param value
             *     allowed object is
             *     {@link HashMap }
             *     
             */
            public void setValue(HashMap value) {
                this.value = value;
            }

        }

    }

}
