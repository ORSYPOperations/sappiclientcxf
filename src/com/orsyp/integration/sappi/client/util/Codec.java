package com.orsyp.integration.sappi.client.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Codec {
	
	public static String decrypt(String input) {
		try {
			DESKeySpec keySpec = new DESKeySpec("peppebeddu".getBytes("UTF8"));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
		
			byte[] cleartext = input.getBytes("UTF8");
			byte[] encrypedPwdBytes = Base64.decodeBase64(cleartext);

			Cipher cipher = Cipher.getInstance("DES"); 
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] plainTextPwdBytes = cipher.doFinal(encrypedPwdBytes);
			return new String (plainTextPwdBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}   
		return null;		
	}

	public static String encrypt(String input) {
		try {
			DESKeySpec keySpec = new DESKeySpec("peppebeddu".getBytes("UTF8"));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
		
			byte[] cleartext = input.getBytes("UTF8");

			Cipher cipher = Cipher.getInstance("DES"); 
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return new String (Base64.encodeBase64(cipher.doFinal(cleartext)));
		} catch (Exception e) {
			e.printStackTrace();
		}   
		return null;
	}

}
